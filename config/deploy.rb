set :application, 'your_app_name'
set :repo_url, 'git@github.com:your_username/your_repo.git'
set :deploy_to, '/var/www/your_app_name'
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/master.key')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Additional configuration options...
```

````ruby
# config/deploy/production.rb

server 'your_server_ip', user: 'your_username', roles: %w{app db web}

# Additional configuration options...
```