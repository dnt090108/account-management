Rails.application.routes.draw do
  resources :payments
  resources :transactions
  resources :accounts
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  devise_for :users, defaults: { format: :json }
end
