class AddPaymentDateAndPaymentMethodToPayments < ActiveRecord::Migration[6.1]
  def change
    add_column :payments, :payment_date, :date
    add_column :payments, :payment_method, :string
  end
end
