class CreatePayments < ActiveRecord::Migration[6.1]
  def change
    create_table :payments, id: :uuid do |t|
      t.decimal :amount
      t.string :description
      t.references :user, null: false, foreign_key: true, type: :uuid
      t.references :account, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
