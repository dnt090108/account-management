class AddStatusAndDescriptionToTransactions < ActiveRecord::Migration[6.1]
  def change
    add_column :transactions, :status, :string
    add_column :transactions, :description, :string
  end
end
