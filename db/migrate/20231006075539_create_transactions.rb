class CreateTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :transactions, id: :uuid do |t|
      t.decimal :amount
      t.references :from_account, foreign_key: { to_table: 'accounts' }, type: :uuid
      t.references :to_account, foreign_key: { to_table: 'accounts' }, type: :uuid
      t.timestamps
    end
  end
end
