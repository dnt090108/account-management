# Features
- Sign in/up/out
- Send an email when user resetting password.
- Send an email when user confirming email.
- Send email when user changed password.
- User create accounts
- Acount management
- Payment management
- Transaction management

# Principle
- Convention over configuration
- Thin controller, thin model
- KISS
- DRY
- SOLID

# Pattern
- Repository
- Decorator
- Observer
- Pub-sub
- Singleton
- Builder
- Command
- Proxy

# Technical
- Docker, CI/CD
- JWT token authention
- Auto generate API docs
- Track table change, restore record data
- Background job management
- Debug, track errors
- Monitor system
- Code quality checking
- Api access control
- System load testing
- Automated scheduled data backup

# Unit test
- Model unit testing
- Controller unit testing
- Service objects unit testing
