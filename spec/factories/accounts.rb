# == Schema Information
#
# Table name: accounts
#
#  id         :uuid             not null, primary key
#  balance    :decimal(, )
#  acc_type   :string
#  user_id    :uuid
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  name       :string
#  active     :boolean
#
FactoryBot.define do
  factory :account do
    balance { "" }
    acc_type { "MyString" }
    user { nil }
  end
end
