# == Schema Information
#
# Table name: transactions
#
#  id               :uuid             not null, primary key
#  amount           :decimal(, )
#  from_account_id  :uuid
#  to_account_id    :uuid
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  status           :string
#  description      :string
#  transaction_type :string
#
FactoryBot.define do
  factory :transaction do
    amount { "" }
    from_acc { nil }
    account { "" }
    to_acc { nil }
    account { "MyString" }
  end
end
