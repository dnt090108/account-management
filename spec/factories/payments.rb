# == Schema Information
#
# Table name: payments
#
#  id             :uuid             not null, primary key
#  amount         :decimal(, )
#  description    :string
#  user_id        :uuid             not null
#  account_id     :uuid             not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  payment_date   :date
#  payment_method :string
#  status         :string
#
FactoryBot.define do
  factory :payment do
    amount { "9.99" }
    description { "MyString" }
    user { nil }
    account { nil }
  end
end
