require 'securerandom'

namespace :devise do
	desc "Generate devise seccret key for jwt"
  	task :generate_secret_key => :environment do
    puts SecureRandom.hex(64)
  end
end
