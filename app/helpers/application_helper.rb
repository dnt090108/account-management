# app/helpers/application_helper.rb
module ApplicationHelper
  def format_currency(amount)
    number_to_currency(amount)
  end
  
  def gravatar_image(email, size: 80)
    gravatar_id = Digest::MD5.hexdigest(email.downcase)
    gravatar_url = "https://www.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: "Gravatar")
  end

  def format_date(date)
    date.strftime("%d/%m/%Y")
  end

  
end