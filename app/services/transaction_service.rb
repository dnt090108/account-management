require 'singleton'
module TransactionService
  class TransactionManager
    include Singleton

    def initialize
      @transactions = []
    end

    def start_transaction
      transaction = Transaction.new
      @transactions << transaction
      transaction
    end

    def commit(transaction)
      transaction.commit
      @transactions.delete(transaction)
    end

    def rollback(transaction)
      transaction.rollback
      @transactions.delete(transaction)
    end
  end

  class Transaction
    attr_accessor :from_account, :to_account, :amount, :description
    
    def initialize(from_account, to_account, amount, description)
      @from_account = from_account
      @to_account = to_account
      @amount = amount
      @description = description
      @status = :pending

    end

    def execute
      puts "Executing transaction...."
    end

    def commit
      @status = :committed
    end

    def rollback
      @status = :rolled_back
    end

    def status
      @status
    end
  end

  class TransactionBuilder
    attr_accessor :from_account, :to_account, :amount, :description

    def initialize
      @from_account = nil
      @to_account = nil
      @amount = nil
      @description = nil
    end

    def from(account)
      @from_account = account
      self
    end

    def to(account)
      @to_account = account
      self
    end

    def with_amount(amount)
      @amount = amount
      self
    end

    def with_description(description)
      @description = description
      self
    end

    def build
      raise "Incomplete transaction data" unless valid?

      Transaction.new(from_account, to_account, amount, description)
    end

    private

    def valid?
      !from_account.nil? && !to_account.nil? && !amount.nil? && !description.nil?
    end
  end
end

# builder = TransactionBuilder.new
# transaction = builder
#                 .from("Account A")
#                 .to("Account B")
#                 .with_amount(100)
#                 .with_description("Transfer")
#                 .build

# transaction.execute
