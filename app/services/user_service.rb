module UserService
  class User
    def initialize(user_repository, rating_repository)
      @user_repository = user_repository
      @rating_repository = rating_repository
    end

    def rate_user(user_id, rating)
      @rating_repository.rate_user(user_id, rating)
    end

    def get_user_rating(user_id)
      @rating_repository.get_user_rating(user_id)
    end

    def get_top_rated_users(limit)
      @rating_repository.get_top_rated_users(limit)
    end

    def get_user_count
      @user_repository.get_user_count
    end

    def get_active_user_count(start_date, end_date)
      @user_repository.get_active_user_count(start_date, end_date)
    end

    def get_user_activity(user_id)
      @user_repository.get_user_activity(user_id)
    end

    def get_user_ranking(user_id)
      @rating_repository.get_user_ranking(user_id)
    end
  end

  class UserRepository
    def get_user_count
      User.count
    end

    def get_active_user_count(start_date, end_date)
      User.where(created_at: start_date..end_date).count
    end

    def get_user_activity(user_id)
      User.find(user_id).activity_logs
    end
  end

  class RatingRepository
    def rate_user(user_id, rating)
      Rating.create(user_id: user_id, rating: rating)
    end

    def get_user_rating(user_id)
      Rating.find_by(user_id: user_id)&.rating
    end

    def get_top_rated_users(limit)
      Rating.group(:user_id).order('AVG(rating) DESC').limit(limit).pluck(:user_id)
    end

    def get_user_ranking(user_id)
      Rating.where('rating > ?', get_user_rating(user_id)).count + 1
    end
  end
end