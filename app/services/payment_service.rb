# app/services/payment_service.rb

# real_gateway = StripePaymentGateway.new
# proxy_gateway = ProxyPaymentGateway.new(real_gateway)
# proxy_gateway.process_payment(100)

module PaymentService
  #require 'stripe'
  #require 'paypal-sdk-rest'

  class Payment
    def initialize(amount, payment_gateway)
      @amount = amount
      @payment_gateway = payment_gateway
    end

    def process_payment
      @payment_gateway.process_payment(amount)
    end
  end

  class PaymentGateway
    def process_payment(amount)
      raise NotImplementedError, "This method must be implemented"
    end
  end

  class StripePaymentGateway < PaymentGateway
    def initialize(stripe_api_key, card_token)
      Stripe.api_key = stripe_api_key
      @card_token = card_token
    end

    def process_payment(amount)
      charge = Stripe::Charge.create(
        amount: amount,
        currency: 'usd',
        source: 'tok_visa'
      )
    end
  end

  class PayPalPaymentGateway < PaymentGateway
    def initialize(client_id, client_secret)
      PayPal::SDK::REST.set_config(
        :mode => "sandbox",
        :client_id => client_id,
        :client_secret => client_secret
      )
    end

    def process_payment(amount)
      payment = PayPal::SDK::REST::Payment.new({
        :intent => "sale",
        :payer => {
          :payment_method => "paypal"
        },
        :transactions => [{
          :amount => {
            :total => amount,
            :currency => "USD"
          }
        }]
      })

      if payment.create
        approval_url = payment.links.find { |link| link.rel == "approval_url" }.href
        puts "Payment link: #{approval_url}"
      else
        puts "Exception: #{payment.error.inspect}"
      end
    end
  end


  class PaymentGatewayProxy < PaymentGateway
    def initialize(real_gateway)
      @real_gateway = real_gateway
    end
 
    def process_payment(amount)   
      if @real_gateway.is_a?(StripePaymentGateway)
        if card_valid?(@real_gateway.card_token).false?
          puts "Card is invalid.You can not make a payment."
          return
        end
      elsif @real_gateway.is_a?(PayPalPaymentGateway)
        if client_secret_valid?(@real_gateway.client_secret)
          puts "Client_secret is invalid.You can not make a payment."
          return
        end
      end
      @real_gateway.process_payment(amount)  
    end
    
    private

    def card_valid?(card_token)
      
      return false
    end

    def client_secret_valid?(client_secret)
      
      return false
    end
  end
end