
module AccountService
  class Account
    def initialize(account_params)
      @account_params = account_params
    end

    def deposit(amount)
      @balance += amount
      puts "Deposited $#{amount} into the account. New balance: $#{@balance}"
    end

    def withdraw(amount)
      if @balance >= amount
        @balance -= amount
        puts "Withdrew $#{amount} from the account. New balance: $#{@balance}"
      else
        puts "Insufficient balance in the account."
      end
    end

    def validate_account_information
      validate_balance
    end

    private

    def validate_balance
      unless balance.is_a?(Numeric) && balance >= 0
        raise "...."
      end
    end
  end

  class Command
    def execute
      raise NotImplementedError, "#{self.class} must implement the 'execute' method."
    end
  end

  class DepositCommand < Command
    def initialize(account, amount)
      @account = account
      @amount = amount
    end

    def execute
      @account.deposit(@amount)
    end
  end

  class WithdrawCommand < Command
    def initialize(account, amount)
      @account = account
      @amount = amount
    end

    def execute
      @account.withdraw(@amount)
    end
  end
end