class WithDrawJob < ApplicationJob
  queue_as :default

  def perform(account_id, amount)
    account = Account.find(account_id)
    withdraw_command = WithDrawCommand.new(account, amount)
    withdraw_command.execute
  end
end

#WithDrawJob.perform_later(account_id, amount)