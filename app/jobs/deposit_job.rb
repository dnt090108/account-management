class DepositJob < ApplicationJob
  queue_as :default

  def perform(account_id, amount)
    account = Account.find(account_id)
    deposit_command = DepositCommand.new(account, amount)
    deposit_command.execute
  end
end

#DepositJob.perform_later(account_id, amount)