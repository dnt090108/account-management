# == Schema Information
#
# Table name: transactions
#
#  id               :uuid             not null, primary key
#  amount           :decimal(, )
#  from_account_id  :uuid
#  to_account_id    :uuid
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  status           :string
#  description      :string
#  transaction_type :string
#
class Transaction < ApplicationRecord
  #enum status: { pending: "pending", committed: "committed", rolled_back: "rolled_back" }
  belongs_to :from_account, class_name: "Account"
  belongs_to :to_account, class_name: "Account"

  validates :amount, presence: true, numericality: { greater_than: 0 }
  validates :transaction_type, presence: true, inclusion: { in: %w[money point] }
  validate :validate_sufficient_balance

  private

  def validate_sufficient_balance
    if transaction_type == "withdrawal" && account.balance < amount
      errors.add(:base, "Insufficient balance for withdrawal")
    end
  end
end
