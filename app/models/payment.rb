# == Schema Information
#
# Table name: payments
#
#  id             :uuid             not null, primary key
#  amount         :decimal(, )
#  description    :string
#  user_id        :uuid             not null
#  account_id     :uuid             not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  payment_date   :date
#  payment_method :string
#  status         :string
#
class Payment < ApplicationRecord
  belongs_to :account

  enum status: { pending: 'pending', completed: 'completed', failed: 'failed' }
  enum payment_method: { credit_card: 'credit_card', debit_card: 'debit_card'}
  
  validates :amount, presence: true, numericality: { greater_than: 0 }
  validates :status, presence: true, inclusion: { in: %w[credit_card debit_card] }
  validates :payment_method, presence: true, inclusion: { in: %w[pending completed failed] }
  validate :validate_payment_date

  private

  def validate_payment_date
    if payment_date.present? && payment_date > Date.today
      errors.add(:payment_date, "cannot be in the future")
    end
  end
end
