# == Schema Information
#
# Table name: accounts
#
#  id         :uuid             not null, primary key
#  balance    :decimal(, )
#  acc_type   :string
#  user_id    :uuid
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  name       :string
#  active     :boolean
#
class Account < ApplicationRecord
  include Audit
  
  enum acc_type: { money: "money", point: "point" }
  belongs_to :user
  has_many :from_transactions, class_name: "Transaction", foreign_key: "from_account_id"
  has_many :to_transactions, class_name: "Transaction", foreign_key: "to_account_id"
  has_many :payments

  validates :balance, numericality: { greater_than_or_equal_to: 0 }
  validate :validate_account_type

  private

  def validate_account_type
    unless %w[money].include?(account_type)
      errors.add(:account_type, "should be either 'money' or 'point'")
    end
  end

end
