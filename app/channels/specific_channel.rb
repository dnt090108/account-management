class SpecificChannel < ApplicationCable::Channel
  def subscribed
    stream_from "specific_channel"
  end

  def send_message(payload)
    ActionCable.server.broadcast("specific_channel", payload)
  end
end

# send notifications 
#SpecificChannel.broadcast_to("specific_channel", message: message)