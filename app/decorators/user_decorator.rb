# app/decorators/user_decorator.rb
class UserDecorator < Draper::Decorator
  delegate_all

  def full_name
    "#{object.first_name} #{object.last_name}"
  end

  def formatted_created_at
    object.created_at.strftime("%B %e, %Y")
  end
end


 # @user = User.find(params[:id]).decorate
 # @user.full_name
 # @user.formatted_created_at